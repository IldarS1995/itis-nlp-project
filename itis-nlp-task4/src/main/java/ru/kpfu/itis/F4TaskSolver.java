package ru.kpfu.itis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.exception.DeadEndException;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.parsing.ArgumentParsingResult;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Решает задачу F4.
 * Алгоритм: каждое предложение начинается на искусственное слово BEGIN. Сначала мы ищем произвольную н-граму,
 * начинающуюся на BEGIN, затем ищем н-граму, начинающуюся на последнее слово первой н-грамы, и так до тех пор,
 * пока последним словом не найден знак пунктуации, на который может оканчиваться предложение.
 * Завершающий знак пунктуации в приоритете - если среди возможных н-грам, начинающихся на данное слово,
 * есть н-грама, заканчивающаяся знаком пунктуации, то выбираем ее, но точку мы ищем не раньше, чем в результат будет добавлено
 * minCountOfNGrams н-грам.
 */
@Component
public class F4TaskSolver implements TaskSolver<List<String>, ArgumentParsingResult> {

    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;
    @Autowired
    private WordsRepositoryImpl wordsRepository;

    private static final Random random = new Random(new Date().getTime());

    /**
     * Минимальное количество н-грам, которое должно быть в сгенерированном предложении
     */
    private static final int minCountOfNGrams = 4;

    /**
     * Слово, идентифицирующее начало предложения.
     */
    private final static Word beginWord = new Word(-10, "<BEGIN>");

    /**
     * Знаки пунктуации, на которые может заканчиваться предложение
     */
    private final static List<String> punctuationMarks = Arrays.asList("...", "?!", ".", "!", "?");

    private static final Logger logger = Logger.getLogger(F4TaskSolver.class);

    public List<String> solveTask(ArgumentParsingResult parsingResult) throws IOException {
        //1. Загнать данные модели в память
        logger.info("Importing data to memory.");
        CorpusInfo corpusInfo = dataImporter.importData(new File(parsingResult.getFileDumpPath()), null);
        logger.info("Imported dump.");

        //2. Сгруппировать н-грамы по начальному слову
        logger.info("Grouping n-grams by their first word.");
        gramsOccurrenceRepository.groupByFirstWord();
        logger.info("Grouped n-grams.");

        //Знаки пунктуации, на который может оканчиваться предложение - например, '.', '?', '!', '...'
        List<Word> punctuationWords = getPunctuationWords();

        //ID'шники завершающих знаков пунктуации
        List<Integer> punctWordsIds = punctuationWords.stream()
                .map(Word::getId)
                .collect(Collectors.toList());

        List<String> resultingSentences = new ArrayList<>();
        for (int k = 0; k < 25; k++) {
            //3. Составляем предложение
            logger.info("Getting list of n-grams starting with <BEGIN>");
            //Находим н-грамы, начинающиеся на <BEGIN>
            List<NGram> nGrams = gramsOccurrenceRepository.findByFirstWord(beginWord.getId());

            //Если true, предложение полностью построено
            boolean sentenceBuilt = false;
            List<NGram> result = null;
            while (!sentenceBuilt) {
                try {
                    result = generateSentence(nGrams, punctWordsIds);
                    sentenceBuilt = true;
                }
                catch (DeadEndException exc) {
                    //Нет таких н-грам, которые начинаются на это слово; начать процесс построения предложения заново
                    logger.info("Dead end; try generating sentence again.");
                }
            }

            //5. Превращаем полученный список в строку
            logger.info("Converting resulting list to a string.");
            String resultSentence = formSentence(result);
            resultingSentences.add(resultSentence);
        }

        return resultingSentences;
    }

    private List<Word> getPunctuationWords() {
        return punctuationMarks.stream()
                .map(mark -> wordsRepository.getWord(mark))
                .filter(word -> word != null)
                .collect(Collectors.toList());
    }

    /**
     * Сгенерировать преложение
     * @param nGrams н-грамы, стоящие в начале предложений
     * @param punctWordsIds Завершающие знаки пунтуации
     * @return Сгенерированное предложение, выраженное в виде списка последовательно идущих н-грам
     */
    private List<NGram> generateSentence(List<NGram> nGrams, List<Integer> punctWordsIds) {
        //Берем рандомом любую из н-грам, начинающихся на <BEGIN>
        NGram nGram = arbitraryGram(nGrams, punctWordsIds);

        logger.info("Generating sentence...");
        boolean foundEnding = false;
        List<NGram> result = new ArrayList<>();
        result.add(nGram);
        while (!foundEnding) {
            Integer lastWord = nGram.getnGramWords().get(nGram.getnGramWords().size() - 1);
            nGrams = gramsOccurrenceRepository.findByFirstWord(lastWord);
            if (nGrams.size() == 0) {
                //Нет таких н-грам, которые начинаются на это слово
                throw new DeadEndException();
            }
            NGram withPoint;
            if (result.size() > minCountOfNGrams && (withPoint = findEndingNGram(nGrams, punctWordsIds)) != null) {
                //Найдена н-грама со знаком пунктуации на конце, закончить формирование предложения
                foundEnding = true;
                nGram = withPoint;
            }
            else {
                nGram = arbitraryGram(nGrams, punctWordsIds);
            }
            result.add(nGram);
        }

        logger.info("Finished generating sentence.");
        return result;
    }

    /**
     * Сформировать предложение из н-грам данного списка. Последнее слово каждой н-грамы - это первое слово следующей н-грамы.
     */
    private String formSentence(List<NGram> result) {
        NGram firstNGram = result.get(0);
        firstNGram.getnGramWords().remove(0);

        NGram lastNGram = result.get(result.size() - 1);
        result.remove(result.size() - 1);

        result.forEach(nGram -> nGram.getnGramWords().remove(nGram.getnGramWords().size() - 1));

        List<Integer> wordsIds = new ArrayList<>();
        result.forEach(nGram -> wordsIds.addAll(nGram.getnGramWords()));

        wordsIds.addAll(lastNGram.getnGramWords());

        return formString(wordsIds);
    }

    /**
     * Сформировать строку из ID'шников слов
     */
    private String formString(List<Integer> wordsIds) {
        StringBuilder sb = new StringBuilder();
        wordsIds.forEach(wordId -> sb.append(wordsRepository.getWord(wordId).getWord()).append(" "));
        return sb.toString();
    }

    /**
     * Найти среди данных н-грам н-граму со знаком пунктуации на конце
     */
    private NGram findEndingNGram(List<NGram> nGrams, List<Integer> punctWordsIds) {
        return nGrams.stream()
                .filter(nGram -> punctWordsIds.contains(nGram.getnGramWords().get(nGram.getnGramWords().size() - 1)))
                .findAny()
                .orElse(null);
    }

    /**
     * Возвращает произвольную н-граму из данного списка, не заканчивающуюся на знак пунктуации.
     * Выбор н-грамы учитывает ее частоту - н-грамы с бОльшей встречаемости имеют бОльшую вероятность быть выбранными:
     * используется класс Random для генерации числа от 0 до суммы встречаемостей всех н-грам, затем все н-грамы располагаются
     * на одном отрезке; длина отрезка, соответствующего определенной н-граме, равна встречаемости этой н-грамы.
     *
     * @throws DeadEndException Если в списке nGrams после изъятия всех н-грам, заканчивающихся на точку,
     *                          не осталось н-грам
     */
    private NGram arbitraryGram(List<NGram> nGrams, List<Integer> punctWordsIds) {
        List<NGram> withoutPointList = nGrams.stream()
                .filter(nGram -> !punctWordsIds.contains(nGram.getnGramWords().get(nGram.getnGramWords().size() - 1)))
                .collect(Collectors.toList());
        if (withoutPointList.size() == 0) {
            //В списке не осталось н-грам, выбирать не из чего
            throw new DeadEndException();
        }

        //Считаем суммарную встречаемость отобранных н-грам
        long countSum = withoutPointList.stream()
                .mapToLong(NGram::getCount)
                .sum();
        //Генерируем произвольное число от 0 до sum
        long num = Math.abs(random.nextLong()) % countSum;

        //Находим ту н-граму, которая на отрезке включает в себя это число, если его расположить на отрезке
        long currSum = 0;
        for (NGram nGram : withoutPointList) {
            if (num <= currSum + nGram.getCount()) {
                return nGram;
            }

            currSum += nGram.getCount();
        }

        //До сюда не должно дойти
        throw new IllegalStateException("Couldn't take an arbitrary n-gram.");
    }
}
