package ru.kpfu.itis.parsing;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.parser.ArgumentParser;

@Component
public class Task4ArgumentParser implements ArgumentParser<ArgumentParsingResult> {

    @Autowired
    private CommandLineParser parser;

    @Override
    public ArgumentParsingResult parseArguments(String[] args) {
        Options options = new Options();
        options.addOption("lm", "language-model", true, "The file with serialized language model.");

        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        }
        catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }

        return new ArgumentParsingResult(cmd.getOptionValue("language-model"));
    }
}
