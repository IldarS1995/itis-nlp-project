package ru.kpfu.itis.parsing;

import ru.kpfu.itis.dto.DtoClass;
import ru.kpfu.itis.dto.NotNullField;

public class ArgumentParsingResult extends DtoClass {

    @NotNullField
    private String fileDumpPath;

    public ArgumentParsingResult(String fileDumpPath) {
        this.fileDumpPath = fileDumpPath;

        checkConstraints();
    }

    public String getFileDumpPath() {
        return fileDumpPath;
    }
}
