package ru.kpfu.itis;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.dto.IllegalFieldValueException;
import ru.kpfu.itis.parsing.ArgumentParsingResult;
import ru.kpfu.itis.parsing.Task4ArgumentParser;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.IOException;
import java.util.List;

/**
 * Точка входа в программу решения задачи F1
 */
@Configuration
@ComponentScan
public class App {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        //Парсим переданные аргументы
        Task4ArgumentParser argumentParser = context.getBean(Task4ArgumentParser.class);
        ArgumentParsingResult result;
        try {
            result = argumentParser.parseArguments(args);
        }
        catch(IllegalFieldValueException exc) {
            System.err.println(exc.getMessage());
            return;
        }

        //Решаем задачу
        F4TaskSolver solver = context.getBean(F4TaskSolver.class);
        try {
            List<String> resultingSentences = solver.solveTask(result);
            System.out.println("Resulting sentences:");
            int i = 0;
            for (String resultingSentence : resultingSentences) {
                System.out.println("#" + ++i + ": ------" + resultingSentence);
            }
        }
        catch (IOException e) {
            System.err.println("IO error: " + e.getMessage());
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.err.println("State error: " + e.getMessage());
        }
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public LineSplitter lineSplitter() {
        return new LineSplitter();
    }

    @Bean
    public DynamicSmoother dynamicSmoother() {
        return new DynamicSmoother();
    }
}