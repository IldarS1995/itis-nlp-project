package ru.kpfu.itis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.exception.CorpusNotFoundException;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.processing.LineSplitter;
import ru.kpfu.itis.processing.SmoothingType;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class F5TaskSolver implements TaskSolver<Double, ArgumentParsingResult> {
    @Autowired
    private PerplexityLogarithmCalculator perplexityLogarithmCalculator;
    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private LineSplitter lineSplitter;

    private Logger logger = Logger.getLogger(F5TaskSolver.class);

    public Double solveTask(ArgumentParsingResult parsingResult)
            throws IOException, CorpusNotFoundException {
        //1. Загнать данные модели в память
        logger.info("Importing data to memory.");
        CorpusInfo corpusInfo = dataImporter.importData(new File(parsingResult.getPathToDump()), null);
        logger.info("Imported dump.");

        LineSplitter.WordType wordType = corpusInfo.getWordType();
        Integer suffixX = corpusInfo.getSuffixX();
        long totalNGramSum = corpusInfo.getTotalNGramSum();

        //Устанавливаем сглаживание, примененное при обучении модели
        //Нужно для определения поведения калькулятора при встрече с незнакомой н-грамой
        SmoothingType smoothingType = null;
        if (corpusInfo.getSmoothing() != null) {
            smoothingType = SmoothingType.valueOf(corpusInfo.getSmoothing().toUpperCase());
        }
        perplexityLogarithmCalculator.setSmoothing(smoothingType);
        perplexityLogarithmCalculator.setTotalNGramCount1Sum(corpusInfo.getTotalNGramCount1Sum());

        File corpFolder = new File(parsingResult.getPathToCorpus());
        if (!corpFolder.isDirectory()) {
            throw new IllegalArgumentException("The test corpus is not a directory.");
        }

        //3. Считать тестовый корпус, загоняя данные построчно в калькулятор perplexity
        logger.info("We have " + corpFolder.listFiles().length + " files in the corpus directory.");
        int i = 0;
        if (corpusInfo.isIgnoreCase()) {
            logger.info("Case of symbols is to be ignored.");
        }
        for (File file : corpFolder.listFiles()) {
            logger.info("Processing file " + i++ + " of total: " + corpFolder.listFiles().length);
            try (BufferedReader reader = new BufferedReader(new
                    InputStreamReader(new FileInputStream(file)))) {
                reader.lines().forEach(line -> {
                    List<String> sentenceWords = lineSplitter.splitLineOnTokens(line, wordType, suffixX);
                    if (corpusInfo.isIgnoreCase()) {
                        //Понизить регистр символов слов
                        sentenceWords = sentenceWords.stream()
                                .map(String::toLowerCase)
                                .collect(Collectors.toList());
                    }
                    perplexityLogarithmCalculator.addFrequencySum(sentenceWords,
                            corpusInfo.getnGramVar(), totalNGramSum);
                });
            }
        }
        if(corpFolder.listFiles().length == 0) {
            //Если нет ни одного файла с текстами
            throw new CorpusNotFoundException("No files found in the corpus folder.");
        }
        logger.info("Successfully processed all test files.");

        //4. Завершить подсчет perplexity
        double logPerplexity = perplexityLogarithmCalculator.finishCalculation();
        return logPerplexity;
    }
}
