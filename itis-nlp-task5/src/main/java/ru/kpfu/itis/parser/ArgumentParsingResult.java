package ru.kpfu.itis.parser;

import ru.kpfu.itis.dto.DtoClass;
import ru.kpfu.itis.dto.NotNullField;

public class ArgumentParsingResult extends DtoClass {

    /**
     * Путь к сериализованному файлу языковой модели
     */
    @NotNullField
    private String pathToDump;

    /**
     * Путь к тестовому корпусу
     */
    @NotNullField
    private String pathToCorpus;

    public ArgumentParsingResult(String pathToDump, String pathToCorpus) {
        this.pathToDump = pathToDump;
        this.pathToCorpus = pathToCorpus;

        checkConstraints();
    }

    public String getPathToDump() {
        return pathToDump;
    }

    public String getPathToCorpus() {
        return pathToCorpus;
    }
}
