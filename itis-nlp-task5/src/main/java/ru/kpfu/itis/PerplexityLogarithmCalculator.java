package ru.kpfu.itis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.hashing.ByteArray;
import ru.kpfu.itis.hashing.StringEncoder;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.SmoothingType;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;

import java.util.List;

/**
 * Подсчет логарифма perplexity. На вход функции addFrequencySum() даются строки тестового корпуса,
 * суммы логарифмов частот н-грам складываются во внутреннюю переменную. При вызове метода
 * finishCalculation() сумма делится на N - количество слов в тестовом корпусе, и возвращается с
 * отрицательным знаком. Т.е., другими словами, возвращается -(1/N)*sum(log(freq(w1w2...wn))) для
 * всех н-грам w1w2...wn.
 */
@Component
public class PerplexityLogarithmCalculator {

    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;
    @Autowired
    private StringEncoder stringEncoder;
    @Autowired
    private DynamicSmoother dynamicSmoother;

    /**
     * Количество слов в тестовом корпусе
     */
    private long countOfWords = 0;

    /**
     * Сумма частот (вероятностей) втречаемых в тестовом корпусе н-грам
     */
    private double sumFrequencies = 0.0;

    /**
     * Кодировка текста
     */
    private String encoding = "UTF-8";

    /**
     * Какое сглаживание применялось для данного корпуса
     */
    private SmoothingType smoothing = null;

    /**
     * Количество н-грам со встречаемостью 1
     */
    private Long totalNGramCount1Sum;

    /**
     * Включить частоты н-грам из этого массива в сумму логарифмов частот для perplexity
     * @param sentenceWords Массив строк предложения
     * @param nGramVar н-грамность модели
     * @param totalNGramSum Сумма встречаемостей всех н-грам
     * @throws IllegalStateException Если применяется сглаживание Гуд-Тьюринга, и не установлено
     * поле <code>totalNGramCount1Sum</code>
     */
    public void addFrequencySum(List<String> sentenceWords, int nGramVar, long totalNGramSum) {
        this.countOfWords += sentenceWords.size();

        for (int i = 0;i <= sentenceWords.size() - nGramVar;i++) {
            StringBuilder sb = new StringBuilder();
            sentenceWords.subList(i, i + nGramVar).forEach(sb::append);
            ByteArray nGramHash;
            nGramHash = stringEncoder.encodeString(sb.toString(), encoding);
            NGram nGram = gramsOccurrenceRepository.findNGram(nGramHash);
            //Если н-грама не найдена (nGram == null), то нужно 'на ходу'
            //применить сглаживание (если оно применялось при обучении (smoothing != null))
            double frequency;
            if (nGram == null) {
                frequency = dynamicSmoother.smooth(smoothing, totalNGramSum, totalNGramCount1Sum);
            }
            else {
                frequency = nGram.getFrequency();
            }

            this.sumFrequencies += Math.log(frequency);
        }
    }

    /**
     * Завершить подсчет perplexity
     */
    public double finishCalculation() {
        return -(1/(double)countOfWords)*sumFrequencies;
    }

    /**
     * Кодировка текста
     */
    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Какое сглаживание применялось для данного корпуса
     */
    public SmoothingType getSmoothing() {
        return smoothing;
    }

    public void setSmoothing(SmoothingType smoothing) {
        this.smoothing = smoothing;
    }

    /**
     * Количество н-грам со встречаемостью 1
     */
    public Long getTotalNGramCount1Sum() {
        return totalNGramCount1Sum;
    }

    public void setTotalNGramCount1Sum(Long totalNGramCount1Sum) {
        this.totalNGramCount1Sum = totalNGramCount1Sum;
    }
}
