package ru.kpfu.itis;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.exception.CorpusNotFoundException;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.parser.Task5ArgumentParser;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.IOException;

@Configuration
@ComponentScan
public class App 
{
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        //Парсим переданные аргументы
        Task5ArgumentParser argumentParser = context.getBean(Task5ArgumentParser.class);
        ArgumentParsingResult result = argumentParser.parseArguments(args);

        //Решаем задачу
        F5TaskSolver solver = context.getBean(F5TaskSolver.class);
        try {
            double logPerplexity = solver.solveTask(result);
            System.out.println("The logarithm of perplexity is: " + logPerplexity);
        }
        catch (IOException e) {
            System.err.println("IO error: " + e.getMessage());
        }
        catch (CorpusNotFoundException|IllegalArgumentException e) {
            System.err.println("State error: " + e.getMessage());
        }
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public LineSplitter lineSplitter() {
        return new LineSplitter();
    }

    @Bean
    public DynamicSmoother dynamicSmoother() {
        return new DynamicSmoother();
    }
}
