package ru.kpfu.itis.processing;

import org.junit.Before;
import org.junit.Test;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.StringMD5Encoder;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class WordsRepositoryImplTest {

    private WordsRepositoryImpl wordsRepository;

    private List<String> words = Arrays.asList("Hello", "World", "Ildar", "Hello", "Ildar", "Hi");

    private double ceilingFrequency = 0.0001;

    /**
     * Слово, идентифицирующее начало предложения.
     */
    private final static Word beginWord = new Word(-10, "<BEGIN>");

    @Before
    public void beforeTest() {
        wordsRepository = new WordsRepositoryImpl(new GramsOccurrenceRepositoryImpl(new StringMD5Encoder()));
    }

    @Test
    public void testInsertWords() {
        wordsRepository.insertWords(words);
        Map<String, Word> wordsMap = wordsRepository.getWordsMap();

        Map<String, Integer> wordsOccurrences = new HashMap<>();
        wordsOccurrences.put("Hello", 2);
        wordsOccurrences.put("Ildar", 2);
        wordsOccurrences.put("World", 1);
        wordsOccurrences.put("Hi", 1);

        assertEquals(wordsMap.size(), 4);
        for (Map.Entry<String, Integer> entry : wordsOccurrences.entrySet()) {
            assertEquals(wordsMap.get(entry.getKey()).getCount(), (int)entry.getValue());
        }
    }

    @Test
    public void testCalculateFrequencies() {
        wordsRepository.insertWords(words);
        wordsRepository.calculateFrequencies();

        Map<String, Word> wordsMap = wordsRepository.getWordsMap();

        Map<String, Double> wordsOccurrences = new HashMap<>();
        wordsOccurrences.put("Hello", 1/3.);
        wordsOccurrences.put("Ildar", 1/3.);
        wordsOccurrences.put("World", 1/6.);
        wordsOccurrences.put("Hi", 1/6.);

        assertEquals(wordsMap.size(), 4);
        for (Map.Entry<String, Double> entry : wordsOccurrences.entrySet()) {
            assertEquals(wordsMap.get(entry.getKey()).getFrequency(), entry.getValue(), ceilingFrequency);
        }
    }

    @Test
    public void testDiscoverUnknownWords() {
        wordsRepository.insertWords(words);
        wordsRepository.calculateFrequencies();
        wordsRepository.discoverUnknownWords(0.17, beginWord); //Убираем два слова с частотой 1/6

        Map<String, Word> wordsMap = wordsRepository.getWordsMap();
        assertEquals(wordsMap.size(), 3);

        Map<String, Double> wordsOccurrences = new HashMap<>();
        wordsOccurrences.put("Hello", 1/3.);
        wordsOccurrences.put("Ildar", 1/3.);
        wordsOccurrences.put("<UNK>", 1/3.);

        for (Map.Entry<String, Double> entry : wordsOccurrences.entrySet()) {
            assertEquals(wordsMap.get(entry.getKey()).getFrequency(), entry.getValue(), ceilingFrequency);
        }
    }
}
