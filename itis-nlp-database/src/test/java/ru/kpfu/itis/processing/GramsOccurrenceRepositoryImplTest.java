package ru.kpfu.itis.processing;

import org.junit.Before;
import org.junit.Test;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.ByteArray;
import ru.kpfu.itis.hashing.StringMD5Encoder;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GramsOccurrenceRepositoryImplTest {

    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;

    private int index = 1;

    private double ceilingFrequency = 0.0001;

    private List<List<Word>> nGrams = Arrays.asList(
            words("Ildar", "goes", "home"),
            words("James", "is", "abroad"),
            words("Jennifer", "likes", "surfing"),
            words("Ildar", "goes", "home")
    );

    @Before
    public void beforeTest() {
        gramsOccurrenceRepository = new GramsOccurrenceRepositoryImpl(new StringMD5Encoder());
    }

    @Test
    public void testSaveNGrams() {
        gramsOccurrenceRepository.saveNGrams(nGrams, "UTF-8");

        Map<ByteArray, NGram> nGramMap = gramsOccurrenceRepository.getnGramMap();
        assertEquals(nGramMap.size(), 3);

        Map<String, Long> countMap = new HashMap<>();
        countMap.put("Ildar goes home", 2L);
        countMap.put("James is abroad", 1L);
        countMap.put("Jennifer likes surfing", 1L);

        for (NGram nGram : nGramMap.values()) {
            assertEquals((long)find(countMap, nGram.getnGramWords()), nGram.getCount());
        }
    }

    /** Найти среди н-грамм ту, в которой слова имеют в точности эти ID'шники
     * и вернуть кол-во встречаемостей этой н-грамы */
    private <T> T find(Map<String, T> countMap, List<Integer> integers) {
        for (List<Word> nGram : nGrams) {
            List<Integer> idx = nGram.stream().map(Word::getId).collect(Collectors.toList());
            if (idx.equals(integers)) {
                return countMap.get(nGram
                        .stream()
                        .map(Word::getWord)
                        .reduce((a, b) -> a + " " + b)
                        .get());
            }
        }
        return null;
    }

    private List<Word> words(String... strs) {
        return Arrays.asList(strs)
                .stream()
                .map(str -> new Word(index++, str))
                .collect(Collectors.toList());
    }

    @Test
    public void testCalculateFrequencies() {
        gramsOccurrenceRepository.saveNGrams(nGrams, "UTF-8");
        gramsOccurrenceRepository.calculateFrequencies();

        Map<ByteArray, NGram> nGramMap = gramsOccurrenceRepository.getnGramMap();

        Map<String, Double> countMap = new HashMap<>();
        countMap.put("Ildar goes home", 1/2.);
        countMap.put("James is abroad", 1/4.);
        countMap.put("Jennifer likes surfing", 1/4.);

        for (NGram nGram : nGramMap.values()) {
            assertEquals(find(countMap, nGram.getnGramWords()), nGram.getFrequency(), ceilingFrequency);
        }
    }

    @Test
    public void testReplaceWordInGrams() {
        gramsOccurrenceRepository.saveNGrams(nGrams, "UTF-8");

        Map<Integer, Word> wordsByIds = new HashMap<>();
        nGrams.stream().forEach(nGram -> nGram.stream().forEach(word -> wordsByIds.put(word.getId(), word)));
        gramsOccurrenceRepository.replaceWordInNGrams(1, 5, wordsByIds);

        gramsOccurrenceRepository.getnGramsList().stream()
                .forEach(nGram -> nGram.getnGramWords().stream()
                        .forEach(wordId -> assertNotEquals((long)wordId, 1)));
    }
}
