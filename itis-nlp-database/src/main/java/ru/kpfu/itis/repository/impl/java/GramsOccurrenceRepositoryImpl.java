package ru.kpfu.itis.repository.impl.java;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.ByteArray;
import ru.kpfu.itis.hashing.StringEncoder;
import ru.kpfu.itis.repository.GramsOccurrenceRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Хранит данные н-грам для обработки в Java
 */
@Repository
public class GramsOccurrenceRepositoryImpl implements GramsOccurrenceRepository {

    /**
     * Список н-грам. Ключ карты - хеш-значение н-грамы
     */
    private Map<ByteArray, NGram> nGramMap = new HashMap<>();

    private StringEncoder stringEncoder;

    private String defaultEncoding = "UTF-8";

    @Autowired
    public GramsOccurrenceRepositoryImpl(StringEncoder stringEncoder) {
        this.stringEncoder = stringEncoder;
    }

    @Override
    public void saveNGrams(List<List<Word>> nGrams, String encoding) {
        this.defaultEncoding = encoding;

        for (List<Word> nGram : nGrams) {
            ByteArray hash = stringEncoder.encodeString(concat(nGram), encoding);
            NGram found = nGramMap.get(hash);
            if (found == null) {
                found = new NGram(hash, new ArrayList<>(ids(nGram)), 1, 0.0);
                nGramMap.put(hash, found);
            }
            else {
                found.incrementCount();
            }
        }
    }

    private List<Integer> ids(List<Word> words) {
        return words.stream().map(Word::getId).collect(Collectors.toList());
    }

    /**
     * Соединить строки в одну
     */
    private String concat(List<Word> strs) {
        StringBuilder sb = new StringBuilder();
        strs.forEach(word -> sb.append(word.getWord()));
        return sb.toString();
    }
    private String concatStrs(List<String> strs) {
        StringBuilder sb = new StringBuilder();
        strs.forEach(sb::append);
        return sb.toString();
    }

    @Override
    public long calculateFrequencies() {
        //1. Подсчитываем общую сумму встречаемостей н-грам
        long sum = nGramMap.entrySet()
                .stream()
                .mapToLong(entry -> entry.getValue().getCount())
                .sum();

        //2. Рассчитываем частоту каждой н-грамы
        nGramMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(nGram -> nGram.setFrequency(nGram.getCount() / (double) sum));

        return sum;
    }

    /**
     * Получить список н-грам
     */
    public Map<ByteArray, NGram> getnGramMap() {
        return nGramMap;
    }

    public void setNGramsMap(Map<ByteArray, NGram> nGramMap) {
        this.nGramMap = nGramMap;
    }

    @Override
    public NGram findNGram(List<String> nGramTokens) {
        ByteArray byteArray = stringEncoder.encodeString(concatStrs(nGramTokens), defaultEncoding);
        return nGramMap.get(byteArray);
    }

    @Override
    public NGram findNGram(ByteArray nGramHash) {
        return nGramMap.get(nGramHash);
    }

    @Override
    public void replaceWordInNGrams(int wordId, int idToSet, Map<Integer, Word> wordsByIds) {
        //н-грамы, хеши которых надо будет перевычислить
        List<NGram> nGramsToRecalculate = new ArrayList<>();
        nGramMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .filter(nGram -> changedNGram(nGram, wordId, idToSet))
                .forEach(nGramsToRecalculate::add);

        for (NGram nGram : nGramsToRecalculate) {
            //У каждой из этих н-грам нужно пересчитать хеш, т.к. он основывается на словах н-грамы
            ByteArray hash = nGram.getHash();
            nGramMap.remove(hash);
            ByteArray newHash = calculateHash(nGram.getnGramWords(), wordsByIds);
            nGram.setHash(newHash);
            nGramMap.put(newHash, nGram);
        }
    }

    /**
     * Вытащить слова с заданными идентификаторами из wordsByIds и вычислить хеш от их конкатенации
     *
     * @param wordIds  Иден-ры слов
     * @param wordsByIds Карта со словами, ключ - ID слова
     */
    private ByteArray calculateHash(List<Integer> wordIds, Map<Integer, Word> wordsByIds) {
        List<Word> toCalc = wordIds.stream()
                .map(wordsByIds::get)
                .collect(Collectors.toList());
        return stringEncoder.encodeString(concat(toCalc), defaultEncoding);
    }

    /**
     * Заменяет в н-граме слово с данным ID, если оно в ней присутствует.
     * Возвращает true, если заменено в н-граме хотя бы одно слово.
     */
    private boolean changedNGram(NGram nGram, int wordId, int idToSet) {
        boolean changed = false;
        for (int i = 0; i < nGram.getnGramWords().size(); i++) {
            int id = nGram.getnGramWords().get(i);
            if (id == wordId) {
                //Нашли слово, которое нужно заменить
                nGram.getnGramWords().remove(i);
                nGram.getnGramWords().add(i, idToSet);
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Вернуть только объекты н-грам (все значения из nGramMap)
     */
    public List<NGram> getnGramsList() {
        return nGramMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    /**
     * Н-грамы, сгруппированные по первому слову. Нужно для задания F4
     */
    private Map<Integer, List<NGram>> groupedByFirstWord;

    /**
     * Сгруппировать н-грамы по первому слову; сформировать внутреннюю карту
     */
    public void groupByFirstWord() {
        groupedByFirstWord = nGramMap.entrySet().stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.groupingBy(nGram -> nGram.getnGramWords().get(0)));
    }

    @Override
    public List<NGram> findByFirstWord(Integer beginWordId) {
        List<NGram> result = groupedByFirstWord.get(beginWordId);
        return result == null ? new ArrayList<>() : result;
    }
}
