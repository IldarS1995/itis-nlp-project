package ru.kpfu.itis.repository.impl.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.repository.WordsRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Хранит данные слов для обработки в Java
 */
@Repository
public class WordsRepositoryImpl implements WordsRepository {

    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;

    private Map<String, Word> wordsMap = new HashMap<>();

    private int number = 1;

    private static final String unkWordStr = "<UNK>";

    @Autowired
    public WordsRepositoryImpl(GramsOccurrenceRepositoryImpl gramsOccurrenceRepository) {
        this.gramsOccurrenceRepository = gramsOccurrenceRepository;
    }

    @Override
    public Map<String, Integer> insertWords(List<String> words) {
        Map<String, Integer> result = new HashMap<>();
        for (String word : words) {
            Word found = wordsMap.get(word);
            int id;
            if (found == null) {
                id = idNumber();
                found = new Word(id, word, 1, 0.0);
                wordsMap.put(word, found);
            }
            else {
                //В wordsMap это слово уже присутствует
                found.incrementCount();
                id = found.getId();
            }

            result.put(word, id);
        }
        return result;
    }

    private int idNumber() {
        return number++;
    }

    /**
     * Вернуть карту слов. Ключ - слово, значение - объект с его данными
     */
    public Map<String, Word> getWordsMap() {
        return wordsMap;
    }

    public void setWordsMap(Map<String, Word> wordsMap) {
        this.wordsMap = wordsMap;
        //Вычисляем максимальный ID в списке
        number = wordsMap.entrySet()
                .stream()
                .mapToInt(e -> e.getValue().getId())
                .max()
                .orElse(0) + 1;
    }

    @Override
    public void calculateFrequencies() {
        //1. Подсчитываем общую сумму встречаемостей слов
        long sum = wordsMap.entrySet()
                .stream()
                .mapToInt(entry -> entry.getValue().getCount())
                .sum();

        //2. Рассчитываем частоту каждого слова
        wordsMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(word -> word.setFrequency(word.getCount() / (double)sum));
    }

    @Override
    public int discoverUnknownWords(double ceilingFrequency, Word beginWord) {
        Word unkWord;
        if ((unkWord = wordsMap.get(unkWordStr)) == null) {
            //UNK-слова нет в таблице
            unkWord = new Word(-1, unkWordStr, 0, 0.0);
            //Помещаем слово <UNK> в коллекцию
            wordsMap.put(unkWord.getWord(), unkWord);
        }

        List<Word> foundWords = wordsMap.entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(w -> w.getFrequency() < ceilingFrequency && !w.getWord().equals(unkWordStr))
                .collect(Collectors.toList());

        //Строим карту, в которой слова находятся по их ID - нужно при замене слов в н-грамах
        Map<Integer, Word> wordsByIds = new HashMap<>();
        wordsMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(word -> wordsByIds.put(word.getId(), word));
        wordsByIds.put(beginWord.getId(), beginWord);

        for (Word foundWord : foundWords) {
            unkWord.setCount(unkWord.getCount() + foundWord.getCount());
            unkWord.setFrequency(unkWord.getFrequency() + foundWord.getFrequency());

            //Удаляем найденное слово из коллекции
            wordsMap.remove(foundWord.getWord());

            //Меняем ID слова с удаленного на <UNK> во всех н-грамах с этим словом
            gramsOccurrenceRepository.replaceWordInNGrams(foundWord.getId(), unkWord.getId(), wordsByIds);
        }

        return foundWords.size();
    }

    @Override
    public Word getWord(String word) {
        return wordsMap.get(word);
    }

    @Override
    public Word getWord(int wordId) {
        return wordsMap.entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(word -> word.getId() == wordId)
                .findAny()
                .orElse(null);
    }

    public List<Word> getWordsList() {
        return wordsMap.entrySet().stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
