package ru.kpfu.itis.repository;

import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.ByteArray;

import java.util.List;
import java.util.Map;

public interface GramsOccurrenceRepository {
    /**
     * Сохранить список н-грам (взятых из одного предложения)
     *
     * @param nGrams   Список н-грам, каждая из которых представлена списком слов
     * @param encoding Кодировка, нужна для вычисления хеша н-грамы
     */
    void saveNGrams(List<List<Word>> nGrams, String encoding);

    /**
     * Посчитать частоты н-грам
     */
    long calculateFrequencies();

    /**
     * Найти н-граму с заданными токенами. Если не найдено, возвращает null.
     */
    NGram findNGram(List<String> nGramTokens);

    /**
     * Найти н-граму с заданным хешем. Если не найдено, возвращает null.
     */
    NGram findNGram(ByteArray nGramHash);

    /**
     * Заменить во всех н-грамах слово с id=wordId на слово с id=idToSet
     *
     * @param wordId   ID слова, которое надо заменить в н-грамах
     * @param idToSet  ID слова, на которое нужно заменить
     */
    void replaceWordInNGrams(int wordId, int idToSet, Map<Integer, Word> wordsByIds);

    /**
     * Найти список н-грам со словом, начинающимся на данное слово
     * @return Список н-грам, если найдены н-грамы, начинающиеся на это слово; в противном случае пустой список.
     */
    List<NGram> findByFirstWord(Integer beginWordId);
}
