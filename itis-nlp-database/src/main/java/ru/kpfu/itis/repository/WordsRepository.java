package ru.kpfu.itis.repository;

import ru.kpfu.itis.domain.Word;

import java.util.List;
import java.util.Map;

public interface WordsRepository {
    /**
     * Вставить слова в хранилище
     * @return Карту с данными словами. Ключ - слово, значение - его ID в хранилище.
     */
    Map<String, Integer> insertWords(List<String> words);

    /**
     * Рассчитать частоты слов на основе их встречаемости
     */
    void calculateFrequencies();

    /**
     * Превращаем в UNK слова, имеющие частоты ниже переданной.
     * @return Количество найденных слов
     */
    int discoverUnknownWords(double ceilingFrequency, Word beginWord);

    /**
     * Получить объект слова по его строковому виду. Возвр. null, если не найдено.
     */
    Word getWord(String word);

    /**
     * Получить объект слова по его ID. Возвр. null, если не найдено.
     */
    Word getWord(int wordId);
}
