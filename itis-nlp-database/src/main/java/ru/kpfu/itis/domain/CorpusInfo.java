package ru.kpfu.itis.domain;

import ru.kpfu.itis.processing.LineSplitter;

import java.io.Serializable;
import java.util.Date;

/**
 * Информация (метаданные) об импортированном корпусе
 */
public class CorpusInfo implements Serializable {

    private Date importDate;

    private int nGramVar;

    private String pathToDump;

    private String uuid;

    private LineSplitter.WordType wordType;

    private Integer suffixX;

    private String smoothing;

    private long totalNGramSum;

    private Long totalNGramCount1Sum;

    private boolean ignoreCase;

    public CorpusInfo(Date importDate, int nGramVar, String pathToDump, String uuid,
                      LineSplitter.WordType wordType, Integer suffixX, String smoothing,
                      long totalNGramSum, Long totalNGramCount1Sum, boolean ignoreCase) {
        this.importDate = importDate;
        this.nGramVar = nGramVar;
        this.pathToDump = pathToDump;
        this.uuid = uuid;
        this.wordType = wordType;
        this.suffixX = suffixX;
        this.smoothing = smoothing;
        this.totalNGramSum = totalNGramSum;
        this.totalNGramCount1Sum = totalNGramCount1Sum;
        this.ignoreCase = ignoreCase;
    }

    /**
     * Дата импорта
     */
    public Date getImportDate() {
        return importDate;
    }

    /**
     * Н-грамность корпуса
     */
    public int getnGramVar() {
        return nGramVar;
    }

    /**
     * Путь к дампу
     */
    public String getPathToDump() {
        return pathToDump;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    /**
     * Уникальный идентификатор данного корпуса
     */
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Характер деления на токены
     */
    public LineSplitter.WordType getWordType() {
        return wordType;
    }

    /**
     * Длина окончаний слов при wordType = SUFFIX_X
     */
    public Integer getSuffixX() {
        return suffixX;
    }

    /**
     * Сглаживание, примененное при обучении модели
     */
    public String getSmoothing() {
        return smoothing;
    }

    /**
     * Сумма всех встречаемостей н-грам в корпусе
     */
    public long getTotalNGramSum() {
        return totalNGramSum;
    }

    /**
     * Количество н-грам со встречаемостью 1. Устанавливается при сглаживании Гуд-Тьюринга.
     */
    public Long getTotalNGramCount1Sum() {
        return totalNGramCount1Sum;
    }

    /**
     * Игнорировать ли регистр текстов
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }
}
