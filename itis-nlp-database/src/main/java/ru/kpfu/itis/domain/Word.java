package ru.kpfu.itis.domain;

import java.io.Serializable;

/**
 * Представляет одно слово из текста
 */
public class Word implements Serializable {
    /**
     * Уникальный ID слова
     */
    private Integer id;
    /**
     * Строковое представление слова
     */
    private String word;
    /**
     * Встречаемость слова в корпусе
     */
    private int count;
    /**
     * Частота слова
     */
    private double frequency;

    public Word(Integer id, String word) {
        this.id = id;
        this.word = word;
    }
    public Word(Integer id, String word, int count, double frequency) {
        this.id = id;
        this.word = word;
        this.count = count;
        this.frequency = frequency;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public void incrementCount() {
        this.count++;
    }
}
