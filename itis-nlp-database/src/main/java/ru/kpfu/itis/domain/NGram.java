package ru.kpfu.itis.domain;

import ru.kpfu.itis.hashing.ByteArray;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Представляет одну н-граму
 */
public class NGram implements Serializable {
    /**
     * Хеш н-грамы, взятый по конкатенации слов, входящих в н-граму
     */
    private ByteArray hash;
    /**
     * Слова, входящие в данную н-граму, именно в таком порядке
     */
    private List<Integer> nGramWords;
    /**
     * Встречаемость данной н-грамы в корпусе
     */
    private long count;
    /**
     * Частота данной н-грамы в корпусе, вычисляемая как отношение встречаемости н-грамы к суммарной встречаемости
     * всех н-грам корпуса.
     */
    private double frequency;

    public NGram(ByteArray hash, List<Integer> nGramWords, long count, double frequency) {
        this.hash = hash;
        this.nGramWords = nGramWords;
        this.count = count;
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NGram nGram = (NGram) o;

        return hash.equals(nGram.hash);
    }

    @Override
    public int hashCode() {
        return hash.hashCode();
    }


    public ByteArray getHash() {
        return hash;
    }

    public void setHash(ByteArray hash) {
        this.hash = hash;
    }

    public List<Integer> getnGramWords() {
        return nGramWords;
    }

    public void setnGramWords(List<Integer> nGramWords) {
        this.nGramWords = nGramWords;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public void incrementCount() {
        this.count++;
    }
}
