package ru.kpfu.itis.export;

import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.exception.DumpAnotherNGramSizeException;

import java.io.File;

/**
 * Импортер сериализованных данных в память
 */
public interface DataImporter {
    /**
     * Импортировать дамп, расположенный по заданному пути
     * @param dumpFilePath Объект, представляющий файл дампа
     * @param nGramsVar Если н-грамность корпуса дампа не совпадает с этой н-грамностью,
     *                  то выбрасывается DumpAnotherNGramSizeException
     * @return Информацию о корпусе
     * @throws DumpAnotherNGramSizeException
     */
    CorpusInfo importData(File dumpFilePath, Integer nGramsVar);
}
