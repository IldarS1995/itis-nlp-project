package ru.kpfu.itis.export;

import ru.kpfu.itis.domain.CorpusInfo;

import java.io.File;

/**
 * Экспорт готовых данных
 */
public interface DataExporter {
    /**
     * Сериализовать данные в указанный файл
     */
    void exportData(CorpusInfo corpusInfo, File filePath);
}
