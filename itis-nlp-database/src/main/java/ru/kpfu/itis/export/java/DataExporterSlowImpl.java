package ru.kpfu.itis.export.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.export.DataExporter;
import ru.kpfu.itis.export.ExportData;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Медленная сериализация - данные выводятся в файл постепенно, а не за раз.
 * Конвертация в сериализованные данные происходит по ходу вывода в файл.
 */
@Component
public class DataExporterSlowImpl implements DataExporter {

    @Autowired
    private WordsRepositoryImpl wordsRepository;
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;

    @Override
    public void exportData(CorpusInfo corpusInfo, File file) {
        ExportData exportData = new ExportData(
                wordsRepository.getWordsMap(),
                gramsOccurrenceRepository.getnGramMap(),
                corpusInfo
        );

        serialize(exportData, file);
    }

    private void serialize(ExportData exportData, File file) {
        try(ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file))) {
            os.writeObject(exportData);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
