package ru.kpfu.itis.export;

import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.ByteArray;

import java.io.Serializable;
import java.util.Map;

/**
 * Данные для сериализации
 */
public class ExportData implements Serializable {
    /**
     * Карта со словами корпуса. Ключ - слово, значение - объект, его представляющий.
     */
    private Map<String, Word> wordsMap;
    /**
     * Карта с н-грамами корпуса. Ключ - хеш н-грамы, взятый по конкатенации ее строк, значение - объект,
     * представляющий н-граму.
     */
    private Map<ByteArray, NGram> nGramMap;
    private CorpusInfo corpusInfo;

    public ExportData(Map<String, Word> wordsMap, Map<ByteArray, NGram> nGramMap, CorpusInfo corpusInfo) {
        this.wordsMap = wordsMap;
        this.nGramMap = nGramMap;
        this.corpusInfo = corpusInfo;
    }

    public Map<String, Word> getWordsMap() {
        return wordsMap;
    }

    public void setWordsMap(Map<String, Word> wordsMap) {
        this.wordsMap = wordsMap;
    }

    public Map<ByteArray, NGram> getnGramMap() {
        return nGramMap;
    }

    public void setnGramMap(Map<ByteArray, NGram> nGramMap) {
        this.nGramMap = nGramMap;
    }

    public CorpusInfo getCorpusInfo() {
        return corpusInfo;
    }

    public void setCorpusInfo(CorpusInfo corpusInfo) {
        this.corpusInfo = corpusInfo;
    }
}
