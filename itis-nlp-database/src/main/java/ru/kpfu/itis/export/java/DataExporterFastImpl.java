package ru.kpfu.itis.export.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.export.DataExporter;
import ru.kpfu.itis.export.ExportData;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.io.*;
import java.nio.file.Files;

/**
 * Быстрая сериализация - данные выводятся в файл не постепенно а за раз.
 * Конвертация в сериализованные данные происходит в памяти.
 */
@Component
public class DataExporterFastImpl implements DataExporter {

    @Autowired
    private WordsRepositoryImpl wordsRepository;
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;

    @Override
    public void exportData(CorpusInfo corpusInfo, File file) {
        ExportData exportData = new ExportData(
                wordsRepository.getWordsMap(),
                gramsOccurrenceRepository.getnGramMap(),
                corpusInfo
        );

        serialize(exportData, file);
    }

    private void serialize(ExportData exportData, File file) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try(ObjectOutputStream os = new ObjectOutputStream(outputStream)) {
            os.writeObject(exportData);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Files.write(file.toPath(), outputStream.toByteArray());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
