package ru.kpfu.itis.export.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.export.ExportData;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.exception.DumpAnotherNGramSizeException;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class DataImporterImpl implements DataImporter {

    @Autowired
    private WordsRepositoryImpl wordsRepository;
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;

    @Override
    public CorpusInfo importData(File dumpFilePath, Integer nGramsVar) {
        if (!dumpFilePath.exists()) {
            throw new IllegalArgumentException("Dump file with the specified path doesn't exist.");
        }

        ExportData exportData = deserialize(dumpFilePath);
        if (nGramsVar != null && exportData.getCorpusInfo().getnGramVar() != nGramsVar) {
            throw new DumpAnotherNGramSizeException("Corpus n-gram variable is not the same as specified in the arguments.");
        }

        wordsRepository.setWordsMap(exportData.getWordsMap());
        gramsOccurrenceRepository.setNGramsMap(exportData.getnGramMap());
        return exportData.getCorpusInfo();
    }

    private ExportData deserialize(File dumpFilePath) {
        try (ObjectInputStream is = new ObjectInputStream(new ByteArrayInputStream(
                            Files.readAllBytes(Paths.get(dumpFilePath.getAbsolutePath()))))) {
            return (ExportData) is.readObject();
        }
        catch (IOException|ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
