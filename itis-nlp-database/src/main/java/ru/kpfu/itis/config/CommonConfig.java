package ru.kpfu.itis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import ru.kpfu.itis.hashing.StringEncoder;
import ru.kpfu.itis.hashing.StringMD5Encoder;

@Configuration
public class CommonConfig {
    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public StringEncoder stringEncoder() {
        return new StringMD5Encoder();
    }
}
