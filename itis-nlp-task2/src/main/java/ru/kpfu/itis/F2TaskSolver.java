package ru.kpfu.itis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.algorithm.TokenOrderRestoration;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class F2TaskSolver implements TaskSolver<String, ArgumentParsingResult> {

    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private LineSplitter lineSplitter;
    @Autowired
    private TokenOrderRestoration tokenOrderRestoration;

    private Logger logger = Logger.getLogger(F2TaskSolver.class);

    public String solveTask(ArgumentParsingResult parsingResult) throws IOException {
        //1. Импортировать сериализованную языковую модель в память
        logger.info("Importing data to memory.");
        CorpusInfo corpusInfo = dataImporter.importData(new File(parsingResult.getDumpFilePath()), null);
        logger.info("Imported dump.");

        //2. Разделяем строку на токены
        List<String> sentenceTokens = lineSplitter.splitLineOnTokens(parsingResult.getInputSentence(),
                corpusInfo.getWordType(), corpusInfo.getSuffixX());

        //3. Восстанавливаем порядок слов в предложении
        logger.info("Starting sentence order restoration process...");
        String result = tokenOrderRestoration.restoreSentence(sentenceTokens, corpusInfo);
        logger.info("Token sequence calculated.");

        return result;
    }
}
