package ru.kpfu.itis.parser;

import ru.kpfu.itis.dto.DtoClass;
import ru.kpfu.itis.dto.NotNullField;

public class ArgumentParsingResult extends DtoClass {

    @NotNullField
    private String dumpFilePath;

    @NotNullField
    private String inputSentence;

    public ArgumentParsingResult(String dumpFilePath, String inputSentence) {
        this.dumpFilePath = dumpFilePath;
        this.inputSentence = inputSentence;

        checkConstraints();
    }

    public String getDumpFilePath() {
        return dumpFilePath;
    }

    public String getInputSentence() {
        return inputSentence;
    }
}
