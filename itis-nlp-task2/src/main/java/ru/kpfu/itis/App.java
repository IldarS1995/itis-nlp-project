package ru.kpfu.itis;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.parser.Task2ArgumentParser;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.IOException;

/**
 * Точка входа в программу решения задачи F2
 */
@Configuration
@ComponentScan
public class App {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        //Парсим переданные аргументы
        Task2ArgumentParser argumentParser = context.getBean(Task2ArgumentParser.class);
        ArgumentParsingResult parsingResult = argumentParser.parseArguments(args);

        //Решаем задачу
        F2TaskSolver solver = context.getBean(F2TaskSolver.class);
        try {
            String output = solver.solveTask(parsingResult);
            System.out.println("The result is: " + output);
        }
        catch (IOException e) {
            System.err.println("IO error: " + e.getMessage());
        }
        catch (IllegalArgumentException e) {
            System.err.println("State error: " + e.getMessage());
        }
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public LineSplitter lineSplitter() {
        return new LineSplitter();
    }

    @Bean
    public DynamicSmoother dynamicSmoother() {
        return new DynamicSmoother();
    }
}
