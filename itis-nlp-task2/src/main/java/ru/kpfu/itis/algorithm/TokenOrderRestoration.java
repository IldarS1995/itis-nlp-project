package ru.kpfu.itis.algorithm;

import ru.kpfu.itis.domain.CorpusInfo;

import java.util.List;

public interface TokenOrderRestoration {
    String restoreSentence(List<String> tokens, CorpusInfo corpusInfo);
}
