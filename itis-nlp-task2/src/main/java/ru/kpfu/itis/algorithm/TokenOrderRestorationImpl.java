package ru.kpfu.itis.algorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.SmoothingType;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

@Component
public class TokenOrderRestorationImpl implements TokenOrderRestoration {
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;
    @Autowired
    private DynamicSmoother dynamicSmoother;

    @Override
    public String restoreSentence(List<String> tokens, CorpusInfo corpusInfo) {
        double maxProbability = 0.0;

        List<String> tokensCopy = new ArrayList<>(tokens);

        List<String> result = new ArrayList<>();
        List<String> intermediateTokens = new ArrayList<>();
        findMaxPermutation(tokensCopy.stream()
                            .map(token -> new Token(token, false))
                            .toArray(Token[]::new),
                intermediateTokens, result, maxProbability, corpusInfo, tokensCopy.size());

        StringBuilder sb = new StringBuilder();
        result.forEach(str -> sb.append(str).append(" "));

        return sb.toString();
    }

    private double findMaxPermutation(Token[] tokens, List<String> intermediateTokens,
                                      List<String> result, double maxProbability,
                                      CorpusInfo corpusInfo, int countFree) {
        if (countFree == 0) {
            double prob = calculateProbability(intermediateTokens, corpusInfo);
            if (prob > maxProbability) {
                result.clear();
                result.addAll(intermediateTokens);
            }
            return prob > maxProbability ? prob : maxProbability;
        }

        double maxProb = maxProbability;
        for (int i = 0;i < tokens.length;i++) {
            if (tokens[i].isTaken()) {
                continue;
            }

            tokens[i].setTaken(true);
            intermediateTokens.add(tokens[i].getToken());
            maxProb = findMaxPermutation(tokens, intermediateTokens, result, maxProb, corpusInfo, countFree - 1);
            intermediateTokens.remove(intermediateTokens.size() - 1);
            tokens[i].setTaken(false);
        }

        return maxProb;
    }

    private class Token {
        private String token;
        private boolean taken;

        public Token(String token, boolean taken) {
            this.token = token;
            this.taken = taken;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isTaken() {
            return taken;
        }

        public void setTaken(boolean taken) {
            this.taken = taken;
        }
    }

    private double calculateProbability(List<String> tokens, CorpusInfo corpusInfo) {
        double sum = 0;
        for (int i = 0; i <= tokens.size() - corpusInfo.getnGramVar(); i++) {
            List<String> nGramTokens = tokens.subList(i, i + corpusInfo.getnGramVar());
            NGram nGram = gramsOccurrenceRepository.findNGram(nGramTokens);
            if (nGram == null) {
                //н-грама не найдена; применить сглаживание "на ходу"
                sum += dynamicSmoother.smooth(SmoothingType.valueOf(corpusInfo.getSmoothing().toUpperCase()),
                        corpusInfo.getTotalNGramSum(),
                        corpusInfo.getTotalNGramCount1Sum());
            }
            else {
                sum += nGram.getFrequency();
            }
        }

        return sum;
    }
}
