package ru.kpfu.itis.parser;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Парсинг аргументов задачи F5
 */
@Component
public class Task3ArgumentParser implements ArgumentParser<ArgumentParsingResult> {

    @Autowired
    private CommandLineParser parser;

    @Override
    public ArgumentParsingResult parseArguments(String[] args) {
        Options options = new Options();
        options.addOption("gn", "guess-num", true, "Count of resulting sentences to form.");
        options.addOption("lm", "language-model", true, "The file with serialized language model.");
        options.addOption("s", "sentence", true, "Input sentence with <SKIP>-words.");

        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        }
        catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }

        return new ArgumentParsingResult(Integer.valueOf(cmd.getOptionValue("guess-num")),
                cmd.getOptionValue("language-model"),
                cmd.getOptionValue("sentence"));
    }
}
