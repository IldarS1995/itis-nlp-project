package ru.kpfu.itis.parser;

import ru.kpfu.itis.dto.DtoClass;
import ru.kpfu.itis.dto.NotNullField;

public class ArgumentParsingResult extends DtoClass {

    /**
     * Путь к сериализованному файлу языковой модели
     */
    @NotNullField
    private String pathToDump;

    /**
     * Путь к тестовому корпусу
     */
    @NotNullField
    private Integer guessNum;

    /**
     * Входное предложение со SKIP-нутыми словами
     */
    @NotNullField
    private String inputSentence;

    public ArgumentParsingResult(Integer guessNum, String pathToDump, String inputSentence) {
        this.pathToDump = pathToDump;
        this.guessNum = guessNum;
        this.inputSentence = inputSentence;

        checkConstraints();
    }

    public String getPathToDump() {
        return pathToDump;
    }

    public Integer getGuessNum() {
        return guessNum;
    }

    public String getInputSentence() {
        return inputSentence;
    }
}
