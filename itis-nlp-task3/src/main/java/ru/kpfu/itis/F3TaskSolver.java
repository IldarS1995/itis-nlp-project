package ru.kpfu.itis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.algorithm.SentenceWordsSelectionService;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.exception.NoSkipWordsException;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class F3TaskSolver implements TaskSolver<List<String>, ArgumentParsingResult> {

    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private LineSplitter lineSplitter;
    @Autowired
    private SentenceWordsSelectionService sentenceWordsSelectionService;

    private Logger logger = Logger.getLogger(F3TaskSolver.class);

    private static final String skipWord = "<skip>";

    public List<String> solveTask(ArgumentParsingResult parsingResult) throws IOException, NoSkipWordsException {
        //1. Проверяем, что <SKIP>-слова присутствуют во входной строке
        if (!parsingResult.getInputSentence().toLowerCase().contains(skipWord)) {
            throw new NoSkipWordsException();
        }

        //2. Импортировать сериализованную языковую модель в память и проверить регистр
        logger.info("Importing the dump to the memory...");
        CorpusInfo corpusInfo = dataImporter.importData(new File(parsingResult.getPathToDump()), null);
        logger.info("Finished importing dump into the memory.");


        String inputSentence = parsingResult.getInputSentence();
        if (corpusInfo.isIgnoreCase()) {
            //Модель была обучена без учета регистра текстов; понизить регистр входного предложения
            inputSentence = inputSentence.toLowerCase();
        }

        //3. Разделяем строку на токены
        List<String> sentenceTokens = lineSplitter.splitLineOnTokens(inputSentence,
                corpusInfo.getWordType(), corpusInfo.getSuffixX());

        //4. Вычисляем предложения
        logger.info("Starting words selection process...");
        List<String> resultingSentences = sentenceWordsSelectionService.generatedSentences(corpusInfo,
                sentenceTokens, parsingResult.getGuessNum());
        logger.info("Sentences calculated.");

        return resultingSentences;
    }
}
