package ru.kpfu.itis.algorithm;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.hashing.ByteArray;
import ru.kpfu.itis.hashing.StringEncoder;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.SmoothingType;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;

import java.util.*;

/**
 * Алгоритм: пусть у нас есть предложение "Серию статей об \SKIP\ я начал со \SKIP\."
 * Алгоритм "закрывает" SKIP-слова по очереди, слева направо.
 * Определение первого SKIP'а: находим все слова, для которых, подставляя их в первый СКИП,
 * существуют н-грамы, содержащие подставленное слово. Т.е. в случае 3-грам если мы выбрали
 * слово "излучении" вместо первого СКИПа, то, значит, существуют н-грамы "статей об излучении",
 * "об излучении я" и "излучении я начал". Далее, из всех таких подходящих слов для первого СКИПа
 * мы выбираем слово, дающий наибольшую сумму вероятностей этих н-грам.
 * Аналогично затем со вторым СКИПом.
 * Если несколько СКИПов стоят настолько близко, что помещаются в одну н-граму, то мы по-прежнему вычисляем
 * СКИПы отдельно и слева направо, а такие н-грамы не учавствуют в общем подсчете
 * вероятности при подстановке слов вместо СКИПа. Пример - в случае 3-грамности и входном предложении "Я пришел SKIP чтобы SKIP"
 * в подсчет суммы вероятностей первого СКИПа войдут только две н-грамы - "Я пришел SKIP" и "пришел SKIP чтобы"
 */
@Service
public class SentenceWordsSelectionServiceImpl implements SentenceWordsSelectionService {

    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;
    @Autowired
    private WordsRepositoryImpl wordsRepository;
    @Autowired
    private StringEncoder stringEncoder;
    @Autowired
    private DynamicSmoother dynamicSmoother;

    private static final String skipWord = "<skip>";

    private static final String encoding = "UTF-8";

    private static final Logger logger = Logger.getLogger(SentenceWordsSelectionServiceImpl.class);

    @Override
    public List<String> generatedSentences(CorpusInfo corpusInfo, List<String> tokens, int sentencesCount) {
        //usedWords - на каких индексах в предложение какие слова уже подставлялись вместо СКИП-слов
        Map<Integer, List<Integer>> usedWords = new HashMap<>();

        //Заполняем usedWords пустыми списками на каждом индексе, соответствующем СКИП-слову
        int i = 0;
        for (String token : tokens) {
            if (token.toLowerCase().equals(skipWord)) {
                usedWords.put(i, new ArrayList<>());
            }
            i++;
        }

        List<String> result = new ArrayList<>();
        for(i = 0;i < sentencesCount;i++) {
            result.add(generateSentence(corpusInfo, tokens, usedWords));
            logger.info("Sentence #" + (i + 1) + " of " + sentencesCount + " has been generated.");
        }
        return result;
    }

    /**
     * Сгенерировать одно предложение, заменив СКИП-слова
     * @param corpusInfo Информация о корпусе
     * @param tokens Токены предложения, содержащие СКИП-слова
     * @param usedWords Слова, которые уже подставлялись вместо СКИП-слов. Слова не должны повторяться на одном индексе
     *                  в разных сгенерированных предложениях.
     */
    private String generateSentence(CorpusInfo corpusInfo, List<String> tokens, Map<Integer, List<Integer>> usedWords) {
        String[] tokensArr = tokens.stream().toArray(String[]::new);
        StringBuilder sentenceBuilder = new StringBuilder();

        for (int i = 0; i < tokens.size(); i++) {
            if (tokensArr[i].toLowerCase().equals(skipWord)) {
                //Нашли СКИП-слово - подбираем слово для его замены в предложении, вставляем в итоговое предложение
                List<Integer> usedWordsOnThisIndex = usedWords.get(i);
                Word foundWord = findWord(corpusInfo, tokensArr, i, usedWordsOnThisIndex);
                sentenceBuilder.append(foundWord.getWord()).append(" ");
                tokensArr[i] = foundWord.getWord();

                usedWordsOnThisIndex.add(foundWord.getId());
            }
            else {
                //Не СКИП-слово, вставляем в итоговое предложение неизмененным
                sentenceBuilder.append(tokensArr[i]).append(" ");
            }
        }
        return sentenceBuilder.toString();
    }

    /**
     * Подобрать и вернуть слово по данному индексу
     *  @param corpusInfo Информация о корпусе
     * @param tokens     Токены предложения
     * @param idx        Индекс СКИП-слова
     * @param usedWordsOnThisIndex Слова, которые уже брались на данном индексе
     */
    private Word findWord(CorpusInfo corpusInfo, String[] tokens, int idx, List<Integer> usedWordsOnThisIndex) {
        double maxProb = 0.0;
        Word maxWord = null;

        for (Word word : wordsRepository.getWordsList()) {
            if (usedWordsOnThisIndex.contains(word.getId())) {
                //Уже использовали это слово при одной из предыдущих генераций предложения
                continue;
            }

            double prob = tryUseWord(corpusInfo, tokens, idx, word);
            if (prob > maxProb) {
                maxProb = prob;
                maxWord = word;
            }
        }

        return maxWord;
    }

    /**
     * Подставить данное слово по данному индексу и посчитать сумму вероятностей
     *
     * @param corpusInfo Информация о корпусе
     * @param tokens     Список токенов предложения
     * @param idx        Индекс, куда надо подставлять в предложении
     * @param word       Слово, которое нужно подставить
     * @return Сумма вероятностей н-грам, содержащих данный индекс и не содержащих другого СКИП-слова
     */
    private double tryUseWord(CorpusInfo corpusInfo, String[] tokens, int idx, Word word) {
        double sumProb = 0.0;
        int nGramVar = corpusInfo.getnGramVar();

        for (int i = Math.max(0, idx - nGramVar + 1); i <= Math.min(tokens.length - nGramVar, idx); i++) {
            if (containsSkip(tokens, i, nGramVar, idx)) {
                //Если в одной н-граме с данным СКИП-словом (по индексу idx) появилось другое СКИП-слово
                break;
            }

            ByteArray hash = getHash(i, nGramVar, tokens, idx, word);
            NGram nGram = gramsOccurrenceRepository.findNGram(hash);
            if (nGram != null) {
                sumProb += nGram.getFrequency();
            }
            else {
                //Если н-грама не найдена, применить сглаживание "на ходу"
                if (corpusInfo.getSmoothing() != null) {
                    sumProb += dynamicSmoother.smooth(SmoothingType.valueOf(corpusInfo.getSmoothing().toUpperCase()),
                            corpusInfo.getTotalNGramSum(),
                            corpusInfo.getTotalNGramCount1Sum());
                }
            }
        }
        return sumProb;
    }

    /**
     * Возвр. true, если в одной н-граме с данным СКИП-словом (по индексу idx) появилось другое СКИП-слово
     *
     * @param tokens   Токены предложения
     * @param i        Начало проверяемой н-грамы
     * @param nGramVar н-грамность
     * @param idx      Индекс, где находится подставляемое СКИП-слово
     */
    private boolean containsSkip(String[] tokens, int i, int nGramVar, int idx) {
        for (int k = i; k < i + nGramVar; k++) {
            if (k != idx && tokens[k].toLowerCase().equals(skipWord)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Взять хеш от н-грамы, начинающейся в tokens с позиции i и имеющей длину nGramVar
     *
     * @param i        Индекс начала н-грамы
     * @param nGramVar Длина n-грамы (переменная n)
     * @param tokens   Токены предложения, откуда брать н-граму
     * @param idx      Индекс, где берется слово word
     * @param word     Слово, которое считать, что находится на позиции idx
     */
    private ByteArray getHash(int i, int nGramVar, String[] tokens, int idx, Word word) {
        StringBuilder sb = new StringBuilder();
        for (int k = i; k < i + nGramVar; k++) {
            if (k == idx) {
                sb.append(word.getWord());
            }
            else {
                sb.append(tokens[k]);
            }
        }

        return stringEncoder.encodeString(sb.toString(), encoding);
    }
}
