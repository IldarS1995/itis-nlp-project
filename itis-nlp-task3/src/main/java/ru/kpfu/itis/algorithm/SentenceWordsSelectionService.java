package ru.kpfu.itis.algorithm;

import ru.kpfu.itis.domain.CorpusInfo;

import java.util.List;

public interface SentenceWordsSelectionService {
    /**
     * Отобрать наиболее вероятные слова и подставить их вместо SKIP в предложении.
     * Всего нужно сгенерировать <code>sentencesCount</code> предложений
     * @param corpusInfo Информация о корпусе, на котором была обучена модель
     * @param tokens Предложение, разделенное на токены
     * @param sentencesCount Количество строк для генерации
     * @return Сгенерированные предложения, со словами вместо SKIP
     */
     List<String> generatedSentences(CorpusInfo corpusInfo, List<String> tokens, int sentencesCount);
}
