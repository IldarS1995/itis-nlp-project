package ru.kpfu.itis;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.exception.NoSkipWordsException;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.parser.Task3ArgumentParser;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.IOException;
import java.util.List;

@Configuration
@ComponentScan
public class App
{
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        //Парсим переданные аргументы
        Task3ArgumentParser argumentParser = context.getBean(Task3ArgumentParser.class);
        ArgumentParsingResult result;
        try {
            result = argumentParser.parseArguments(args);
        }
        catch (NumberFormatException exc) {
            //Если вместо числа в одном из аргументов введена нечисловая строка
            System.err.println("Input error - please enter a valid number in guess-num.");
            return;
        }

        //Решаем задачу
        F3TaskSolver solver = context.getBean(F3TaskSolver.class);
        try {
            List<String> resultingSentences = solver.solveTask(result);
            System.out.println("The resulting sentences:");
            int i = 0;
            for (String resultingSentence : resultingSentences) {
                System.out.println("#" + ++i + ": " + resultingSentence);
            }
        }
        catch (IOException e) {
            System.err.println("IO error: " + e.getMessage());
        }
        catch (NoSkipWordsException e) {
            System.err.println("No <SKIP>-words in the input string.");
        }
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public LineSplitter lineSplitter() {
        return new LineSplitter();
    }

    @Bean
    public DynamicSmoother dynamicSmoother() {
        return new DynamicSmoother();
    }
}