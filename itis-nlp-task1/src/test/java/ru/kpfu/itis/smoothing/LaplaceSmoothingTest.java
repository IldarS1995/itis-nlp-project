package ru.kpfu.itis.smoothing;

import org.junit.Test;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.hashing.ByteArray;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class LaplaceSmoothingTest {

    private LaplaceSmoothing laplaceSmoothing = new LaplaceSmoothing();

    private List<NGram> nGrams = Arrays.asList(
            new NGram(new ByteArray("a".getBytes()), null, 10, 10/17.),
            new NGram(new ByteArray("b".getBytes()), null, 5, 5/17.),
            new NGram(new ByteArray("c".getBytes()), null, 2, 2/17.)
    );

    @Test(expected = IllegalArgumentException.class)
    public void testSmoothNGrams_ParamIsNull() {
        laplaceSmoothing.smoothNGrams(null);
    }

    @Test
    public void testSmoothNGrams() {
        laplaceSmoothing.smoothNGrams(nGrams);
        assertEquals(11, nGrams.get(0).getCount());
        assertEquals(6, nGrams.get(1).getCount());
        assertEquals(3, nGrams.get(2).getCount());
    }
}
