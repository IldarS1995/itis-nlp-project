package ru.kpfu.itis.smoothing;

import org.junit.Test;
import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.hashing.ByteArray;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GoodTuringSmoothingTest {

    private GoodTuringSmoothing goodTuringSmoothing = new GoodTuringSmoothing();

    //N(c): N(1) = 4, N(2) = 2, N(3) = 5
    private List<NGram> nGrams = Arrays.asList(
            new NGram(new ByteArray("a".getBytes()), null, 1, 0),
            new NGram(new ByteArray("b".getBytes()), null, 1, 0),
            new NGram(new ByteArray("c".getBytes()), null, 1, 0),
            new NGram(new ByteArray("d".getBytes()), null, 1, 0),
            new NGram(new ByteArray("e".getBytes()), null, 2, 0),
            new NGram(new ByteArray("f".getBytes()), null, 2, 0),
            new NGram(new ByteArray("g".getBytes()), null, 5, 0)
    );

    @Test(expected = IllegalArgumentException.class)
    public void testSmoothNGrams_ParamIsNull() {
        goodTuringSmoothing.smoothNGrams(null);
    }

    @Test
    public void testSmoothNGrams() {
        goodTuringSmoothing.smoothNGrams(nGrams);

        double[] results = {1,1,1,1,3,3,5};
        int i = 0;
        for (NGram nGram : nGrams) {
            assertEquals(results[i++], nGram.getCount(), 0.001);
        }
    }
}
