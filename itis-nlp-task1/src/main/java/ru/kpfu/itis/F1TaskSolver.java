package ru.kpfu.itis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.CorpusInfo;
import ru.kpfu.itis.domain.Word;
import ru.kpfu.itis.exception.CorpusNotFoundException;
import ru.kpfu.itis.exception.DumpAnotherNGramSizeException;
import ru.kpfu.itis.export.DataExporter;
import ru.kpfu.itis.export.DataImporter;
import ru.kpfu.itis.export.java.DataExporterFastImpl;
import ru.kpfu.itis.export.java.DataExporterSlowImpl;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.processing.LineSplitter;
import ru.kpfu.itis.repository.impl.java.GramsOccurrenceRepositoryImpl;
import ru.kpfu.itis.repository.impl.java.WordsRepositoryImpl;
import ru.kpfu.itis.smoothing.GoodTuringSmoothing;
import ru.kpfu.itis.smoothing.LaplaceSmoothing;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Класс с логикой для решения задачи F1
 */
@Component
public class F1TaskSolver implements TaskSolver<CorpusInfo, ArgumentParsingResult> {

    @Autowired
    private WordsRepositoryImpl wordsRepository;
    @Autowired
    private GramsOccurrenceRepositoryImpl gramsOccurrenceRepository;
    @Autowired
    private DataExporterSlowImpl dataExporterSlow;
    @Autowired
    private DataExporterFastImpl dataExporterFast;
    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private LaplaceSmoothing laplaceSmoothing;
    @Autowired
    private GoodTuringSmoothing goodTuringSmoothing;
    @Autowired
    private LineSplitter lineSplitter;

    private final static Logger logger = Logger.getLogger(F1TaskSolver.class);

    /**
     * Если суммарный размер файлов в корпусе ниже этого порога, использовать быструю сериализацию.
     * В противном случае использовать медленную.
     */
    private static final long megabytesThreshold = 50;

    /**
     * Слово, идентифицирующее начало предложения.
     */
    private final static Word beginWord = new Word(-10, "<BEGIN>");

    /**
     * Решить задачу F1
     * @throws IllegalArgumentException Если входной директории не существует или это не директория
     * @throws CorpusNotFoundException Если не найдены файлы для обучения модели в указанной папке
     * @throws IOException Если произошла ошибка ввода/вывода
     */
    public CorpusInfo solveTask(ArgumentParsingResult parsingResult)
            throws IOException, CorpusNotFoundException {

        //Проверяем существование папки с корпусом
        File corpFolder = new File(parsingResult.getSrcTexts());
        if (!corpFolder.exists()) {
            throw new IllegalArgumentException("Input directory doesn't exist.");
        }
        if (!corpFolder.isDirectory()) {
            throw new IllegalArgumentException("Input element is not a directory.");
        }

        int nGramsVar = parsingResult.getnGramsVariable();

        //1. Импортируем дамп в память, если он существует по этому пути
        File outputFile = new File(parsingResult.getOutput());
        if (outputFile.exists()) {
            try {
                logger.info("Output file (dump) exists. Importing it...");
                dataImporter.importData(outputFile, nGramsVar);
                logger.info("Imported dump.");
            }
            catch (DumpAnotherNGramSizeException e) {
                logger.error("Fail importing existing dump: " + e.getMessage());
                logger.info("Deleting previous dump file..");
                outputFile.delete();
                logger.info("Deleted.");
            }
            catch (Exception e) {
                //Не удалось загрузить дамп, удаляем этот файл
                logger.error("Couldn't load the dump, deleting dump file...");
                outputFile.delete();
                logger.info("Deleted.");
            }
        }

        //2. Заполняем списки слов и н-грам
        logger.info("We have " + corpFolder.listFiles().length + " files in the corpus directory.");
        if (parsingResult.getIgnoreCase()) {
            logger.info("Ignoring case of symbols of the corpus.");
        }
        int i = 0;
        long filesLength = 0; //Суммарный размер всех файлов корпуса, в байтах
        for (File file : corpFolder.listFiles()) {
            filesLength += file.length();
            logger.info("Processing file " + i++ + " of total: " + corpFolder.listFiles().length);
            try (BufferedReader reader = new BufferedReader(new
                    InputStreamReader(new FileInputStream(file), parsingResult.getTextEncoding()))) {
                reader.lines().forEach(line -> {
                    List<String> tokens = lineSplitter.splitLineOnTokens(line.trim(),
                            parsingResult.getWordType(), parsingResult.getX());
                    if (parsingResult.getIgnoreCase()) {
                        //Делаем регистр у символов в токенах нижним
                        tokens = tokens.stream()
                                .map(String::toLowerCase)
                                .collect(Collectors.toList());
                    }
                    //Обрабатываем предложение, извлекая из него n-грамы, только если кол-во слов больше n-грамности
                    if (tokens.size() >= nGramsVar) {
                        //Вставить каждый из токенов в wordsRepository
                        Map<String, Integer> words = wordsRepository.insertWords(tokens);
                        words.put(beginWord.getWord(), beginWord.getId());
                        //Обработать предложение
                        processSentence(tokens, nGramsVar, words, parsingResult.getTextEncoding());
                    }
                });
            }
        }
        if(corpFolder.listFiles().length == 0) {
            //Если нет ни одного файла с текстами
            throw new CorpusNotFoundException("No files found in the input corpus folder.");
        }
        logger.info("Successfully processed all files.");
        logger.info("Count of n grams: " + gramsOccurrenceRepository.getnGramMap().size());

        //Количество н-грам с встречаемостью 1
        Long totalNGramCount1Sum = null;
        //3. Применяем один из методов сглаживания, если он указан
        if (parsingResult.isLaplace()) {
            logger.info("Performing Laplace smoothing");
            laplaceSmoothing.smoothNGrams(gramsOccurrenceRepository.getnGramsList());
            logger.info("Smoothing successfully performed.");
        }
        else if (parsingResult.isGoodTuring()) {
            logger.info("Performing Good-Turing smoothing");
            goodTuringSmoothing.smoothNGrams(gramsOccurrenceRepository.getnGramsList());
            totalNGramCount1Sum = goodTuringSmoothing.getnGramsClasses().get(1L);
            logger.info("Smoothing successfully performed.");
        }
        else {
            logger.info("Smoothing is not performed.");
        }

        //4. Рассчитываем частоты встречаемости н-грам в таблице n_grams_occurrence
        logger.info("Calculating frequencies of n-grams.");
        long nGramsSum = gramsOccurrenceRepository.calculateFrequencies();

        //5. Рассчитываем частоты встречаемости слов
        logger.info("Calculating frequencies of words.");
        wordsRepository.calculateFrequencies();

        //6. Превращаем в <UNK> слова, имеющие частоты ниже переданной
        if (parsingResult.getUnknownWordFrequency() != null) {
            logger.info("Discovering 'unknown' words.");
            int unkWordsCount = wordsRepository.discoverUnknownWords(parsingResult.getUnknownWordFrequency(), beginWord);
            logger.info(unkWordsCount + " 'unknown' words discovered and replaced by <UNK>.");
        }

        //8. Создаем дамп-файл нашего корпуса
        logger.info("Creating serialized dump...");
        String smoothing = parsingResult.isLaplace()
                ? "laplace"
                : (parsingResult.isGoodTuring()
                    ? "good_turing"
                    : null);
        CorpusInfo corpusInfo = new CorpusInfo(
                null,
                nGramsVar,
                outputFile.getAbsolutePath(),
                UUID.randomUUID().toString(),
                parsingResult.getWordType(),
                parsingResult.getX(),
                smoothing,
                nGramsSum,
                totalNGramCount1Sum,
                parsingResult.getIgnoreCase()
        );

        long mbytesSize = filesLength / 1024 / 1024;
        if (mbytesSize < megabytesThreshold) {
            logger.info("Serializing using fast serializer.");
            dataExporterFast.exportData(corpusInfo, outputFile);
        }
        else {
            logger.info("Serializing using slow serializer.");
            dataExporterSlow.exportData(corpusInfo, outputFile);
        }
        logger.info("Created.");

        return corpusInfo;
    }

    /**
     * Вытащить n-грамы из предложения и записать их в хранилище
     */
    private void processSentence(List<String> tokens, int nGramsVar, Map<String, Integer> words, String encoding) {
        List<List<Word>> nGrams = new ArrayList<>();

        //Добавляем в самое начало предложения слово <BEGIN>, означающее начало предложения
        tokens.add(0, beginWord.getWord());

        for (int i = 0; i <= tokens.size() - nGramsVar; i++) {
            List<String> nGram = tokens.subList(i, i + nGramsVar);
            List<Word> nGramWords = nGram.stream().map(word -> new Word(words.get(word), word)).collect(Collectors.toList());
            nGrams.add(nGramWords);
        }

        gramsOccurrenceRepository.saveNGrams(nGrams, encoding);
    }
}
