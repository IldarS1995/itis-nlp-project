package ru.kpfu.itis.parser;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.processing.LineSplitter;

/**
 * Парсинг аргументов задачи F1
 */
@Component
public class Task1ArgumentParser implements ArgumentParser<ArgumentParsingResult> {

    @Autowired
    private CommandLineParser parser;

    public ArgumentParsingResult parseArguments(String[] args) {
        Options options = new Options();
        options.addOption("st", "src-texts", true, "The source folder with texts");
        options.addOption("te", "text-encoding", true, "The text encoding");
        options.addOption("wt", "word-type", true, "The word type");
        options.addOption("n", true, "The n gram variability");
        options.addOption("l", "laplace", false, "Whether to use Laplace flattening");
        options.addOption("gt", "good-turing", false, "Whether to use Good-Turing flattening");
        options.addOption("uwf", "unknown-word-freq", true, "Word frequency under which a word is considered unknown");
        options.addOption("o", "output", true, "Where to save the dump of the language model");
        options.addOption("ic", "ignore-case", false, "Ignore case of symbols in text.");

        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        }
        catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }

        String srcTexts = cmd.getOptionValue("src-texts");

        String textEncoding = cmd.getOptionValue("text-encoding");

        LineSplitter.WordType wordType = LineSplitter.WordType.SURFACE_ALL;
        Integer x = null;
        String wordTypeStr = cmd.getOptionValue("word-type");
        if (cmd.hasOption("word-type")) {
            if (!wordTypeStr.startsWith("suffix_")) {
                wordType = Enum.valueOf(LineSplitter.WordType.class, wordTypeStr.toUpperCase());
            }
            else {
                //word-type = suffix_X, заполнить переменную x в result,
                //перед этим проверив, что x - положительное число
                String xStr = wordTypeStr.substring("suffix_".length());
                if (!StringUtils.isNumeric(xStr) || xStr.equals("0")) {
                    throw new IllegalArgumentException("X variable in --word-type=suffix_X is not positive numeric.");
                }

                x = Integer.parseInt(xStr);
                wordType = LineSplitter.WordType.SUFFIX_X;
            }
        }

        Integer nGramVariable = null;
        String nOption = cmd.getOptionValue("n");
        if (nOption != null && StringUtils.isNumeric(nOption)) {
            nGramVariable = Integer.parseInt(nOption);
        }

        boolean laplace = false;
        boolean goodTuring = false;
        if (cmd.hasOption("laplace")) {
            laplace = true;
        }
        else if (cmd.hasOption("good-turing")) {
            goodTuring = true;
        }

        Double unknownWordFreq = null;
        String freqOption = cmd.getOptionValue("unknown-word-freq");
        if (freqOption != null && NumberUtils.isNumber(freqOption)) {
            unknownWordFreq = Double.parseDouble(freqOption);
        }

        String output = cmd.getOptionValue("o");

        boolean ignoreCase = false;
        if (cmd.hasOption("ignore-case")) {
            ignoreCase = true;
        }

        return new ArgumentParsingResult(
                srcTexts,
                nGramVariable,
                unknownWordFreq,
                output,
                textEncoding == null ? "UTF-8" : textEncoding,
                wordType,
                laplace,
                goodTuring,
                x,
                ignoreCase
        );
    }
}
