package ru.kpfu.itis.parser;

import ru.kpfu.itis.dto.DtoClass;
import ru.kpfu.itis.dto.NotNullField;
import ru.kpfu.itis.processing.LineSplitter;

public class ArgumentParsingResult extends DtoClass {

    /** Путь к корпусу */
    @NotNullField
    private String srcTexts;

    /** Кодировка текста в файлах корпуса. */
    @NotNullField
    private String textEncoding;

    /** Как брать слова из токенов. */
    @NotNullField
    private LineSplitter.WordType wordType;

    /** Число X при word-type = suffix_X. В остальных случаях <code>null</code>. */
    private Integer x;

    /** n-грамность */
    @NotNullField
    private Integer nGramsVariable;

    /** Нужно ли сглаживание по Лапласу. По умолчанию нет. */
    private boolean laplace = false;

    /** Нужно ли сглаживание Гуд-Тьюринга. По умолчанию нет. */
    private boolean goodTuring = false;

    /** Частота, ниже которой слова в обуч. множестве считаются неизвестными */
    private Double unknownWordFrequency;

    /** Куда сохранять сериализованную языковую модель */
    @NotNullField
    private String output;

    /**
     * Игнорировать ли регистр символов в тексте
     */
    private boolean ignoreCase;

    public ArgumentParsingResult(String srcTexts, Integer nGramsVariable, Double unknownWordFrequency,
                                 String output, String textEncoding, LineSplitter.WordType wordType,
                                 boolean laplace, boolean goodTuring, Integer x, boolean ignoreCase) {
        this.srcTexts = srcTexts;
        this.textEncoding = textEncoding;
        this.wordType = wordType;
        this.nGramsVariable = nGramsVariable;
        this.laplace = laplace;
        this.goodTuring = goodTuring;
        this.unknownWordFrequency = unknownWordFrequency;
        this.output = output;
        this.x = x;
        this.ignoreCase = ignoreCase;

        checkConstraints();
    }

    public String getSrcTexts() {
        return srcTexts;
    }

    public String getTextEncoding() {
        return textEncoding;
    }

    public LineSplitter.WordType getWordType() {
        return wordType;
    }

    public Integer getnGramsVariable() {
        return nGramsVariable;
    }

    public boolean isLaplace() {
        return laplace;
    }

    public boolean isGoodTuring() {
        return goodTuring;
    }

    public Double getUnknownWordFrequency() {
        return unknownWordFrequency;
    }

    public String getOutput() {
        return output;
    }

    public Integer getX() {
        return x;
    }

    public boolean getIgnoreCase() {
        return ignoreCase;
    }
}
