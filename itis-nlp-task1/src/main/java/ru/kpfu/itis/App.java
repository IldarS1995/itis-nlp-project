package ru.kpfu.itis;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.*;
import ru.kpfu.itis.exception.CorpusNotFoundException;
import ru.kpfu.itis.parser.ArgumentParsingResult;
import ru.kpfu.itis.parser.Task1ArgumentParser;
import ru.kpfu.itis.processing.LineSplitter;

import java.io.IOException;

/**
 * Точка входа в программу решения задачи F1
 */
@Configuration
@ComponentScan
public class App {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        //Парсим переданные аргументы
        Task1ArgumentParser argumentParser = context.getBean(Task1ArgumentParser.class);
        ArgumentParsingResult result = argumentParser.parseArguments(args);

        //Решаем задачу
        F1TaskSolver solver = context.getBean(F1TaskSolver.class);
        try {
            solver.solveTask(result);
        }
        catch (IOException e) {
            System.err.println("IO error: " + e.getMessage());
        }
        catch (CorpusNotFoundException|IllegalArgumentException e) {
            System.err.println("State error: " + e.getMessage());
        }
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public LineSplitter lineSplitter() {
        return new LineSplitter();
    }
}
