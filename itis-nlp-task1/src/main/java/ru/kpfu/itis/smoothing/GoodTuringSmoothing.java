package ru.kpfu.itis.smoothing;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.NGram;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Сглаживание по Гуд-Тьюрингу
 */
@Component
public class GoodTuringSmoothing implements Smoothing {

    private Logger logger = Logger.getLogger(GoodTuringSmoothing.class);

    private Map<Long, Long> nGramsClasses;

    @Override
    public void smoothNGrams(List<NGram> nGrams) {
        if (nGrams == null) {
            throw new IllegalArgumentException("N-grams shouldn't be null.");
        }

        logger.info("Good Turing smoothing is being applied to the n-grams of the corpus.");

        //1. Разбиваем все слова по классам в зависимости от их встречаемости в корпусе
        //Ключ - встречаемость н-грамы, значение - сколько н-грам имеют данную встречаемость
        nGramsClasses = nGrams.stream()
                .collect(Collectors.groupingBy(NGram::getCount, Collectors.counting()));

        //Линейная регрессия для предсказания Nc в алгоритме, если Nc = 0
        SimpleRegression regression = new SimpleRegression();
        logger.info("Using simple (linear) regression for Good-Turing discounting.");
        //2. Обучаем линейную регрессию
        for (Map.Entry<Long, Long> entry : nGramsClasses.entrySet()) {
            double c = (double) entry.getKey();
            double Nc = (double) entry.getValue();
            regression.addData(Math.log(c), Math.log(Nc));
        }
        logger.info("Regression learned.");

        //3. Пересчитываем встречаемость каждого слова
        for (NGram nGram : nGrams) {
            long occurrenceCount = nGram.getCount();
            Long classCountPlus1 = nGramsClasses.get(occurrenceCount + 1);
            double NcPlus1; // Nc+1
            if (classCountPlus1 == null) {
                //Нет класса встречаемости occurrenceCount + 1
                double predictionNcPlus1 = regression.predict(Math.log(occurrenceCount + 1));
                NcPlus1 = Math.pow(Math.E, predictionNcPlus1);
                logger.info("No class " + (occurrenceCount + 1) + "; performing prediction: " + NcPlus1);
            }
            else {
                NcPlus1 = classCountPlus1;
            }

            double adjustedVal = ((occurrenceCount + 1) * NcPlus1
                    / (double) nGramsClasses.get(occurrenceCount));
            nGram.setCount((long) Math.ceil(adjustedVal));
        }
    }

    /**
     * Классы н-грам: ключ - встречаемость, значение - сколько н-грам имеют данную встречаемость
     */
    public Map<Long, Long> getnGramsClasses() {
        return nGramsClasses;
    }
}
