package ru.kpfu.itis.smoothing;

import ru.kpfu.itis.domain.NGram;
import ru.kpfu.itis.domain.Word;

import java.util.List;

/**
 * Сглаживание (изменение количеств появления н-грам определенным алгоритмом)
 */
public interface Smoothing {
    /**
     * Произвести сглаживание данных н-грам
     * @throws IllegalArgumentException Если параметр nGrams равен null.
     */
    void smoothNGrams(List<NGram> nGrams);
}
