package ru.kpfu.itis.smoothing;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.domain.NGram;

import java.util.List;

/**
 * Сглаживание по Лапласу
 */
@Component
public class LaplaceSmoothing implements Smoothing {

    private Logger logger = Logger.getLogger(LaplaceSmoothing.class);

    @Override
    public void smoothNGrams(List<NGram> nGrams) {
        if (nGrams == null) {
            throw new IllegalArgumentException("N-grams shouldn't be null.");
        }

        logger.info("Laplace smoothing is being applied to the n-grams of the corpus.");
        nGrams.forEach(NGram::incrementCount);
    }
}
