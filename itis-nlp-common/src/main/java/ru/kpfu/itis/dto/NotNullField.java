package ru.kpfu.itis.dto;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Поле не должно быть null
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface NotNullField {
}
