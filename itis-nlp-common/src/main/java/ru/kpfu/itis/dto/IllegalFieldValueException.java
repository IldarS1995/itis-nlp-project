package ru.kpfu.itis.dto;

public class IllegalFieldValueException extends RuntimeException {

    public IllegalFieldValueException(String message) {
        super(message);
    }
}
