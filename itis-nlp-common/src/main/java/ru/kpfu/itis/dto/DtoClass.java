package ru.kpfu.itis.dto;

import java.lang.reflect.Field;

public abstract class DtoClass {

    /**
     * Проверить все ограничения полей в данном DTO-классе - например, поля, помеченные @NotNullField
     *
     * @throws IllegalFieldValueException Если одно из обязательных полей является <code>null</code>.
     */
    protected void checkConstraints() {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(NotNullField.class)) {
                field.setAccessible(true);
                Object value;
                try {
                    value = field.get(this);
                }
                catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }

                if (value == null) {
                    throw new IllegalFieldValueException("Поле " + field.getName() + " не инициализировано.");
                }
            }
        }
    }
}
