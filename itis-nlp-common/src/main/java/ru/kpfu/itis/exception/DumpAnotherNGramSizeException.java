package ru.kpfu.itis.exception;

/** У дампа, который мы пытаемся импортировать, не та же самая н-грамность */
public class DumpAnotherNGramSizeException extends RuntimeException {
    public DumpAnotherNGramSizeException(String message) {
        super(message);
    }
}
