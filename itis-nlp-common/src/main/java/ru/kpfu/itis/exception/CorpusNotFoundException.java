package ru.kpfu.itis.exception;

/** Корпус не найден */
public class CorpusNotFoundException extends Exception {
    public CorpusNotFoundException(String message) {
        super(message);
    }
}
