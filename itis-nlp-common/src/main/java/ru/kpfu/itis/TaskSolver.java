package ru.kpfu.itis;

import ru.kpfu.itis.dto.DtoClass;

/**
 * Решатель определенной задачи
 * @param <T> Тип возвращаемых данных
 * @param <V> Тип входных данных
 */
public interface TaskSolver<T, V extends DtoClass> {
    /**
     * Решает задачу и возвращает результат решения
     * @param inputData Данные, необходимые для решения задачи.
     */
    T solveTask(V inputData) throws Exception;
}
