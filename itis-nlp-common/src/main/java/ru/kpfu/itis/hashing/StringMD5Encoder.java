package ru.kpfu.itis.hashing;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** Рассчитывает хеш-значение строки алгоритмом MD5 */
public class StringMD5Encoder implements StringEncoder {
    @Override
    public ByteArray encodeString(String str, String encoding) {
        byte[] strBytes = null;
        try {
            strBytes = str.getBytes(encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            //Не должно выпасть
            throw new RuntimeException(e);
        }
        return new ByteArray(md.digest(strBytes));
    }
}
