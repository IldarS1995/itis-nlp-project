package ru.kpfu.itis.hashing;

import java.io.UnsupportedEncodingException;

/**
 * Превращает строку в ее хеш-значение
 */
public interface StringEncoder {
    /**
     * Получить хеш-значение строки
     *  @param str Строка, хеш-значение которой нужно получить
     * @param encoding Кодировка строки
     */
    ByteArray encodeString(String str, String encoding);
}
