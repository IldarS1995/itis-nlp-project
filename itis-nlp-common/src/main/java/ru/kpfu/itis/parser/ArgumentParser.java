package ru.kpfu.itis.parser;

import ru.kpfu.itis.dto.DtoClass;

public interface ArgumentParser <T extends DtoClass> {
    /** Распарсить переданные аргументы, заполнив Java-объект */
    T parseArguments(String[] args);
}
