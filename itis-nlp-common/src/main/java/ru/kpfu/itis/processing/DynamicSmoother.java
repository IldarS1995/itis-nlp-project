package ru.kpfu.itis.processing;

public class DynamicSmoother {
    public double smooth(SmoothingType smoothing, long totalNGramSum, Long totalNGramCount1Sum) {
        double frequency;
        if (smoothing == null) {
            //логарифм от 0 получится бесконечность, в итоге получим бесконечное perplexity
            frequency = 0;
        }
        else if (smoothing.equals(SmoothingType.LAPLACE)) {
            frequency = 1.0 / totalNGramSum;
        }
        else {
            //Гуд-Тьюринга
            //Используем частоту н-грам, встретившихся 1 раз, для того чтобы
            //оценить частоту н-грам, не встретившихся ни разу
            if (totalNGramCount1Sum == null) {
                throw new IllegalStateException("Good Turing smoothing failed because " +
                        "the total count of n-grams that occurred once is null.");
            }
            frequency = (double)totalNGramCount1Sum / totalNGramSum;
        }

        return frequency;
    }
}
