package ru.kpfu.itis.processing;

import opennlp.tools.stemmer.snowball.SnowballStemmer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Разделитель строк на токены
 */
public class LineSplitter {
    public enum WordType implements Serializable { SURFACE_ALL, SURFACE_NO_PM, STEM, SUFFIX_X };

    private static final List<String> punctuationMarks =
            Arrays.asList("...", ".", ",", ";", "?", "!", ":", "-", "'", "`", "—");

    /**
     * Разделить строку на токены
     * @param line Строка, которую надо разделить
     * @param wordType Тип токенов
     * @param suffixX длина окончаний слов при wordType = SUFFIX_X
     * @return Разделенная строка
     */
    public List<String> splitLineOnTokens(String line, WordType wordType, Integer suffixX) {
        String[] split = line.split(" ");
        switch (wordType) {
            case SURFACE_ALL:
                return extractPunctuationMarks(Arrays.asList(split));
            case SURFACE_NO_PM:
                return extractPunctuationMarks(Arrays.asList(split))
                        .stream()
                        .map(this::removePunctuationMarks)
                        .filter(str -> str != null && !str.isEmpty())
                        .collect(Collectors.toList());
            case SUFFIX_X:
                return extractPunctuationMarks(Arrays.asList(split))
                        .stream()
                        .map(str -> getSuffixX(str, suffixX))
                        .collect(Collectors.toList());
            default: //STEM
                SnowballStemmer snowballStemmer = new SnowballStemmer(SnowballStemmer.ALGORITHM.RUSSIAN);
                return extractPunctuationMarks(Arrays.asList(split))
                        .stream()
                        .map(str -> (String) snowballStemmer.stem(str))
                        .collect(Collectors.toList());
        }
    }

    /**
     * Из каждого слова вычленяет знаки пунктуации. Например, слово 'Привет.как' будет
     * разделено на три отдельных - 'Привет', '.' и 'как'.
     * @param words Слова предложения, разделенные пробелом
     */
    private List<String> extractPunctuationMarks(List<String> words) {
        List<String> result = new ArrayList<>();
        for (String word : words) {
            int lastWordStart = 0;
            for (int i = 0;i < word.length();i++) {
                String symbol = String.valueOf(word.charAt(i));
                if (punctuationMarks.contains(symbol)) {
                    if (i != lastWordStart) {
                        result.add(word.substring(lastWordStart, i));
                    }
                    result.add(symbol);
                    lastWordStart = i + 1;
                }
            }
            if (lastWordStart != word.length()) {
                result.add(word.substring(lastWordStart));
            }
        }

        return result;
    }

    private String removePunctuationMarks(String str) {
        for (String punct : punctuationMarks) {
            str = str.replace(punct, "");
        }
        return str;
    }

    private String getSuffixX(String str, Integer suffixX) {
        return str.length() - suffixX < 0 ? str : str.substring(str.length() - suffixX);
    }
}
