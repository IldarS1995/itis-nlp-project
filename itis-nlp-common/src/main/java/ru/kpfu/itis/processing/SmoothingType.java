package ru.kpfu.itis.processing;

public enum SmoothingType {
    LAPLACE, GOOD_TURING
};
