package ru.kpfu.itis.processing.processing;

import org.junit.Test;
import ru.kpfu.itis.processing.DynamicSmoother;
import ru.kpfu.itis.processing.SmoothingType;

import static junit.framework.TestCase.assertEquals;

public class DynamicSmootherTest {

    private DynamicSmoother dynamicSmoother = new DynamicSmoother();

    @Test
    public void testSmooth_noSmoothing() {
        assertEquals(0.0, dynamicSmoother.smooth(null, 0, null));
    }

    @Test
    public void testSmooth_laplace() {
        double[][] tests = {{0.5, 2.0}, {1/3., 3.0}, {1.0, 1.0}};
        for (double[] test : tests) {
            assertEquals(test[0], dynamicSmoother.smooth(SmoothingType.LAPLACE, (long)test[1], null), 0.001);
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testSmooth_goodTuring_Class1IsNull() {
        dynamicSmoother.smooth(SmoothingType.GOOD_TURING, 50, null);
    }

    @Test
    public void testSmooth_goodTuring() {
        double[][] tests = {{0.5, 2, 1}, {0.5, 6, 3}, {0.2, 5, 1}};
        for (double[] test : tests) {
            assertEquals(test[0], dynamicSmoother.smooth(SmoothingType.GOOD_TURING, (long)test[1], (long)test[2]), 0.001);
        }
    }
}
