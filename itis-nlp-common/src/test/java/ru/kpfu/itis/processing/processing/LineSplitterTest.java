package ru.kpfu.itis.processing.processing;

import org.junit.Assert;
import org.junit.Test;
import ru.kpfu.itis.processing.LineSplitter;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LineSplitterTest {

    private LineSplitter lineSplitter = new LineSplitter();

    private List<String> testData = Arrays.asList(
            "Привет мир!Как дела?",
            "Сегодня был дождь, и поэтому я не пошел гулять.",
            "Экзамен - самое лучшее,что со мной произошло!"
    );

    @Test
    public void testSplit_SurfaceAll() {
        List<List<String>> result = Arrays.asList(
                Arrays.asList("Привет", "мир", "!", "Как", "дела", "?"),
                Arrays.asList("Сегодня", "был", "дождь", ",", "и", "поэтому", "я", "не", "пошел", "гулять", "."),
                Arrays.asList("Экзамен", "-", "самое", "лучшее", ",", "что", "со", "мной", "произошло", "!")
        );

        for(int i = 0;i < testData.size();i++) {
            String testStr = testData.get(i);
            List<String> expected = result.get(i);
            assertListEquals(expected, lineSplitter.splitLineOnTokens(testStr, LineSplitter.WordType.SURFACE_ALL, null));
        }
    }

    private void assertListEquals(List<String> expected, List<String> strings) {
        for (int i = 0;i < expected.size();i++) {
            assertEquals(expected.get(i), strings.get(i));
        }
    }

    @Test
    public void testSplit_SurfaceNoPm() {
        List<List<String>> result = Arrays.asList(
                Arrays.asList("Привет", "мир", "Как", "дела"),
                Arrays.asList("Сегодня", "был", "дождь", "и", "поэтому", "я", "не", "пошел", "гулять"),
                Arrays.asList("Экзамен", "самое", "лучшее", "что", "со", "мной", "произошло")
        );

        for(int i = 0;i < testData.size();i++) {
            String testStr = testData.get(i);
            List<String> expected = result.get(i);
            assertListEquals(expected, lineSplitter.splitLineOnTokens(testStr, LineSplitter.WordType.SURFACE_NO_PM, null));
        }
    }

    @Test
    public void testSplit_SuffixX() {
        List<List<String>> result = Arrays.asList(
                Arrays.asList("ивет", "мир", "!", "Как", "дела", "?"),
                Arrays.asList("одня", "был", "ождь", ",", "и", "тому", "я", "не", "ошел", "лять", "."),
                Arrays.asList("амен", "-", "амое", "чшее", ",", "что", "со", "мной", "ошло", "!")
        );

        for(int i = 0;i < testData.size();i++) {
            String testStr = testData.get(i);
            List<String> expected = result.get(i);
            assertListEquals(expected, lineSplitter.splitLineOnTokens(testStr, LineSplitter.WordType.SUFFIX_X, 4));
        }
    }

    @Test
    public void testSplit_Stem() {
        List<List<String>> result = Arrays.asList(
                Arrays.asList("Привет", "мир", "!", "Как", "дел", "?"),
                Arrays.asList("Сегодн", "был", "дожд", ",", "и", "поэт", "я", "не", "пошел", "гуля", "."),
                Arrays.asList("Экзам", "-", "сам", "лучш", ",", "что", "со", "мно", "произошл", "!")
        );

        for(int i = 0;i < testData.size();i++) {
            String testStr = testData.get(i);
            List<String> expected = result.get(i);
            assertListEquals(expected, lineSplitter.splitLineOnTokens(testStr, LineSplitter.WordType.STEM, null));
        }
    }
}
